﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestClassEventManager : MonoBehaviour
{
    //private int health;
    //private bool isDead;
    //public int Health { get { return health; } set { isDead = health <= 0; } }

    // Start is called before the first frame update
    void Start()
    {
        EventManager.SubscribeEvent(EventID.Test1, DebugSubscribe);
    }

    void DebugSubscribe()
    {
        Debug.Log("Subbed");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EventManager.TriggerEvent(EventID.Test1);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EventID
{
    Test1 = 0,
    test2 = 1,
    test3 = 2
}

public static class EventManager
{
    private static Dictionary<EventID, Action> eventHandler = new Dictionary<EventID, Action>();

    public static void SubscribeEvent(EventID eventID, Action action)
    {
        if (!eventHandler.ContainsKey(eventID))
        {
            eventHandler.Add(eventID,null);
        }
        eventHandler[eventID] += action;
    }

    public static void UnSubsribeEvent(EventID eventID, Action action)
    {
        if (eventHandler.ContainsKey(eventID))
        {
            eventHandler[eventID] -= action;
        }
    }

    public static void TriggerEvent(EventID eventID)
    {
        if (eventHandler.ContainsKey(eventID))
            eventHandler[eventID]?.Invoke();
    }

}
